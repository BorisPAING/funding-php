<?php

include('inc/Funding.class.php');
include('config.php');

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Exemple de Funding PHP</title>
		<link rel="stylesheet" type="text/css" href="funding.css" />
	</head>
	<body>
		
		<h1><?php echo _("Coucou"); ?></h1>
		
		<p>
			Nous sommes Axiom-Team et nous œuvrons à la promotion de la monnaie libre Ğ1. 
		</p>
		
		<p>
			On prend la parole ici parce qu'on voulait vous informer de la chose suivante&nbsp;:
		</p>
		
		<p>
			Il existe une caisse de côtisations qui permet à la communauté de valoriser (en monnaie libre) 
			la contribution que les développeurs de Cesium apportent au développement de la Ğ1.
		</p>
		
		<p>
			L'objectif est de réunir sur cette caisse la somme de <?php echo FUNDING_TARGET; ?> DU<sub>Ğ1</sub> sur 30 jours glissant.
		</p>
		
		<p>
			Voilà où nous en sommes aujourd'hui ce mois-ci par rapport à cet objectif&nbsp;:
		</p>

		
		<?php
		$startDate = date('d/m/Y', (time() - (30*24*3600)));
		$target = 100;
		$funding = new Funding(FUNDING_PUBKEY, $target, $startDate, 'relative');
		
		echo '
		<aside class="crowdfunding-widget">
			<meter min="0" max="100" value="'. $funding->getPercentage() .'" high="75" low="25" class="progress-bar">
				'. $funding->getPercentage() .'%
			</meter>
			
			<p>
				<strong>'. $funding->getPercentage() .'%</strong>
				<span>du montant souhaité est atteint</span>
			</p>

			<p>
				<strong>'. $funding->getAmountDonated() . ' DU<sub>Ğ1</sub></strong>
				<span>ont déjà donnés, sur un total de '. $target .' DU<sub>Ğ1</sub></span>
			</p>

			<p>
				<span>grâce à </span>
				<strong>'. $funding->getDonorsNb() . '</strong>
				<span>donateurs</span>
			</p>
		</aside>
		';
		
		?>
		
		
		<p>
			Si vous souhaitez soutenir le projet Cesium, c'est simple : 
		</p>

		<div id="pubkey-and-copy-button">
			<p class="pubkey">
				Copiez la clef suivante : 

				<input id="pubkey" type="text" value="<?php echo FUNDING_PUBKEY; ?>" size="8" />... 
				
				dans votre presse-papier en cliquant sur le bouton ci-dessous :
			</p>

			<p class="CTA-button">
				<button id="copyButton">
					Copier la clef
				</button>
			</p>

			<div id="successMsg">
				<p>Et maintenant collez-la dans Cesium afin de faire votre don 😉</p>
				<p style="text-align: center;">Merci pour votre générosité ❤️</p>
				<p style="text-align: right;">Axiom-Team</p>
			</div>
		</div>
	

		<script>
		function copy() {

			var copyText = document.querySelector("#pubkey");
			copyText.select();
			document.execCommand("copy");

			var successMsg = document.querySelector("#successMsg");
			successMsg.style.opacity = "1";
			/*successMsg.style.height = "3em";*/

			var copyButton = document.querySelector("#copyButton");
			copyButton.style.animation = "none";

		}

		function support() {

			var pubkeyAndCopyButton = document.querySelector("#pubkey-and-copy-button");
			var supportButtonContainer = document.querySelector("#supportButtonContainer");
			supportButtonContainer.style.opacity = "0";
			supportButtonContainer.style.height = "0";
			pubkeyAndCopyButton.style.height = "100%";
			pubkeyAndCopyButton.style.opacity = "1";

			var supportButton = document.querySelector("#supportButton");
			$(this).style.animation = "none";
		}

		document.querySelector("#copyButton").addEventListener("click", copy);
		document.querySelector("#supportButton").addEventListener("click", support);
		</script>
		
	</body>
</html>